#!/bin/bash
#	Author: Joe Castagneri				Date: August 6, 2016
# Running this script installs vim by moving the dot files to the proper locations and 
# installing packages. Note that it will overwrite existing dotfiles.

# Warn user and receive permission for install

IFS=$'\n'
# Color prints
FAILED=`tput setaf 1`
SUCCESS=`tput setaf 2`
NC=`tput setaf 4`
RESET=`tput sgr0`

declare -a SUCCESSFUL_COMMAND_LIST
declare -a FAILED_COMMAND

echo "This script will overwrite existing zsh configs during installation."
while true; do
	read -p "Are you sure that you want to continue? (y/n) :" yn
	case $yn in
		[Yy]* ) break;;
		[Nn]* ) echo "Goodbye!"; exit;;
		* ) echo "Please answer y/es or n/o"
	esac
done

exitScript()
{
    COUNTER=0

	echo ""
	echo "${NC}Script Summary: ${RESET}"
	#cycle through all the successful commands
	for i in ${SUCCESSFUL_COMMAND_LIST[@]}
	do
		echo "${SUCCESS} SUCCESS: ${RESET}" "${SUCCESSFUL_COMMAND_LIST[COUNTER]}"
		((COUNTER+=1))
	done

	for i in ${FAILED_COMMAND[@]}
	do
		echo "${FAILED} FAILED:  ${RESET}" "${FAILED_COMMAND[0]}"
		echo ""
		echo "${FAILED}Failed Command:${RESET}"
		echo " ${FAILED_COMMAND[1]}"
		echo ""
		echo "${FAILED}Script failure. Exiting...${RESET}"
		echo ""
		exit 1
	done

	echo "${SUCCESS}zsh environment installed successfully${RESET}"
	exit 0
}

checkActionResult()
{
	echo "${NC}$2... ${RESET}"

	eval $1 # Run the command

	if [ $? -eq 0 ]
	then
		echo "${SUCCESS} SUCCESS: $2 ${RESET}"
		SUCCESSFUL_COMMAND_LIST+=($2)
	else
		echo "${FAILED} FAILED: $2 ${RESET}"
		FAILED_COMMAND+=($2)
		FAILED_COMMAND+=($1)
		exitScript
	fi
}
ZSH_CMD='for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do; ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"; done'

checkActionResult 'cp runcoms/zpreztorc ~' 'Preparing config'
ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
checkActionResult 'cd' 'Changing to home directory'
checkActionResult 'sudo apt-get -y install zsh' 'Checking zsh is installed'
checkActionResult 'git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"' 'Cloning prezto'
checkActionResult "$ROOT_DIR/configure_zprezto.sh" 'Installing default configs'
checkActionResult 'mv zpreztorc .zpreztorc' 'Installing custom config'
checkActionResult 'chsh -s /bin/zsh' 'Setting zsh as default shell'
checkActionResult 'git clone https://github.com/powerline/fonts.git' 'Cloning powerline font repo'
checkActionResult './fonts/install.sh' 'Installing fonts'
echo "Successfully installed zsh. Restart shell." 
